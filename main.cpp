#pragma GCC optimize(3)

#include <unistd.h>
#include <bits/stdc++.h>
#include <iostream>
#include <string>
#include <bitset>

using namespace std;

// Store 16 subkeys
bitset<48> Keys[16];

// Initial Permutation (IP) table
int T1[] = {58, 50, 42, 34, 26, 18, 10, 2,
            60, 52, 44, 36, 28, 20, 12, 4,
            62, 54, 46, 38, 30, 22, 14, 6,
            64, 56, 48, 40, 32, 24, 16, 8,
            57, 49, 41, 33, 25, 17, 9,  1,
            59, 51, 43, 35, 27, 19, 11, 3,
            61, 53, 45, 37, 29, 21, 13, 5,
            63, 55, 47, 39, 31, 23, 15, 7
           };

// Key replacement table��64-bit key->56-bit
int T2[] = {57, 49, 41, 33, 25, 17, 9,
            1, 58, 50, 42, 34, 26, 18,
            10,  2, 59, 51, 43, 35, 27,
            19, 11,  3, 60, 52, 44, 36,
            3, 55, 47, 39, 31, 23, 15,
            7, 62, 54, 46, 38, 30, 22,
            14,  6, 61, 53, 45, 37, 29,
            21, 13,  5, 28, 20, 12,  4
           };

// Left shift table
int T3[] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

// Compression permutation: 56-bit key -> 48-bit key
int T4[] = {14, 17, 11, 24,  1,  5,
            3, 28, 15,  6, 21, 10,
            23, 19, 12,  4, 26,  8,
            16,  7, 27, 20, 13,  2,
            41, 52, 31, 37, 47, 55,
            30, 40, 51, 45, 33, 48,
            44, 49, 39, 56, 34, 53,
            46, 42, 50, 36, 29, 32
           };

// Extention displacement table, 32-bit -> 48-bit
int T5[] = {32,  1,  2,  3,  4,  5,
            4,  5,  6,  7,  8,  9,
            8,  9, 10, 11, 12, 13,
            12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21,
            20, 21, 22, 23, 24, 25,
            24, 25, 26, 27, 28, 29,
            28, 29, 30, 31, 32,  1
           };

// Inverse IP table
int T6[] = {40, 8, 48, 16, 56, 24, 64, 32,
            39, 7, 47, 15, 55, 23, 63, 31,
            38, 6, 46, 14, 54, 22, 62, 30,
            37, 5, 45, 13, 53, 21, 61, 29,
            36, 4, 44, 12, 52, 20, 60, 28,
            35, 3, 43, 11, 51, 19, 59, 27,
            34, 2, 42, 10, 50, 18, 58, 26,
            33, 1, 41,  9, 49, 17, 57, 25
           };

// Sbox, 4x16, 6-bit -> 4-bit
int S[8][4][16] =
{
    {
        {14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7},
        {0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8},
        {4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0},
        {15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13}
    },
    {
        {15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},
        {3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},
        {0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},
        {13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}
    },
    {
        {10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8},
        {13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1},
        {13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7},
        {1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}
    },
    {
        {7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},
        {13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},
        {10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},
        {3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}
    },
    {
        {2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9},
        {14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6},
        {4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14},
        {11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}
    },
    {
        {12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11},
        {10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8},
        {9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6},
        {4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}
    },
    {
        {4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1},
        {13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6},
        {1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2},
        {6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}
    },
    {
        {13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7},
        {1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2},
        {7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8},
        {2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}
    }
};

// P-permutation table��32-bit -> 32-bit
int P[] = {16,  7, 20, 21,
           29, 12, 28, 17,
           1, 15, 23, 26,
           5, 18, 31, 10,
           2,  8, 24, 14,
           32, 27,  3,  9,
           19, 13, 30,  6,
           22, 11,  4, 25
          };

/**
 * Cipher function f(R,K)��Receive 32-bit data and 48-bit key to produce a 32-bit output
 */
bitset<32> f(bitset<32> R, bitset<48> k)
{

    bitset<48> expand_r;
    // Step 1: Extenion displacement, 32 -> 48
    for(int i=0; i<48; i++)
        expand_r[47-i] = R[32-T5[i]];
    cout<<"The result of 32-bit data -> 48-bit: "<<expand_r.to_string()<<endl;

    // Step 2: XOR operation
    expand_r = expand_r ^ k;
    cout<<"The result of XOR operation (key and extented data): "<<expand_r.to_string()<<endl;

    // Step 3: Search the S-box
    bitset<32> output;
    int x = 0;
    for(int i=0; i<48; i=i+6)
    {
        int row = expand_r[47-i]*2 + expand_r[47-i-5];
        int col = expand_r[47-i-1]*8 + expand_r[47-i-2]*4 + expand_r[47-i-3]*2 + expand_r[47-i-4];
        int num = S[i/6][row][col];
        bitset<4> binary(num);
        output[31-x] = binary[3];
        output[31-x-1] = binary[2];
        output[31-x-2] = binary[1];
        output[31-x-3] = binary[0];
        x += 4;
    }
    cout<<"S-box permutation result (48->32): "<<output.to_string()<<endl;

    // Step 4: P-permutation��32 -> 32
    bitset<32> tmp = output;
    for(int i=0; i<32; i++)
        output[31-i] = tmp[32-P[i]];
    cout<<"P-permutation result (32->32): "<<output.to_string()<<endl;
    return output;
}


/**
 *
 * Initial key permutation, 64 -> 48
 *
 */
bitset<56> key_first_init(bitset<64> bin_key)
{
    bitset<56> result;
    for (int i = 0; i < 56; i++)
    {
        result[55-i] = bin_key[64 - T2[i]];
    }
    return result;
}


/**
 *
 *Key left shift
 *
 */
bitset<28> key_left_move(int move,bitset<28> k)
{
    bitset<28> tmp = k;
    for(int i=27; i>=0; --i)
    {
        k[i] = i-move<0 ? tmp[i-move+28] : tmp[i-move];
    }
    return k;
}


/**
 *
 *
 * Key compression permutation
 *
 */
bitset<48> compress_key(bitset<56> key)
{
    bitset<48> result;
    for (int i = 0; i < 48; i++)
    {
        result[47-i] = key[56 - T4[i]];
    }
    return result;

}

/**
 *  Produce 16 48-bit subkeys
 */
void generateKeys(bitset<64> bin_key)
{
    cout<<"16 48-bit subkeys: "<<endl;
    // Initial displacement of key, 64 bit -> 56 bit
    bitset<56> key = key_first_init(bin_key);
    cout<<"Initial displacement of key: "<<key.to_string()<<endl;
    // Generate subkeys, store in Keys[16]
    for(int i=0; i < 16; i++)
    {
        // Get the first 28-bit and last 28-bit
        bitset<28> left;
        bitset<28> right;
        for(int i=28; i<56; i++)
            left[i-28] = key[i];
        for(int i=0; i<28; i++)
            right[i] = key[i];

        // Left shift
        left = key_left_move(T3[i],left);
        right = key_left_move(T3[i],right);
        for(int i=28; i<56; i++)
            key[i] = left[i-28];
        for(int i=0; i<28; i++)
            key[i] = right[i];
        // Compression permutation, 56-bit -> 48-bit
        Keys[i] = compress_key(key);
        cout<<"key"<<(i+1)<<":"<<Keys[i]<<endl;
    }
}

/**
 * Binary string -> Hex string
*/
string bin_2_hex(bitset<64> bin)
{
    string result="";
    for (int i = bin.size() - 1; i >= 0 ; i-=4)
    {
        // Decimal number
        int dec = bin[i]*8 + bin[i-1]*4 + bin[i-2]*2 + bin[i-3];
        if(dec < 10)
        {
            result += (char)(dec + '0');
        }
        else
        {
            result += (char)(dec + 'A' - 10);
        }
    }
    return result;
}

// Hex string-> Binary string
bitset<64> hex_2_bin(string hex)
{

    string result = "";
    bitset<64> d;
    for (int i = 0; i < hex.length(); i++)
    {
        // Hexadecimal
        // Hex -> Dec
        int temp = 0;
        if(hex[i] >= '0' && hex[i] <= '9')
        {
            temp = hex[i] - '0';
        }
        else if(hex[i] >= 'a' && hex[i] <= 'z')
        {
            temp = hex[i] - 'a' + 10;
        }
        else if(hex[i] >= 'A' && hex[i] <= 'Z')
        {
            temp = hex[i] - 'A' + 10;
        }
        else
        {
            cout<<"Error converting hexadecimal string to hexadecimal string, non-hexadecimal character!"<<hex[i]<<endl;
            return d;
        }

        // Decimal -> Binary  4-bit, High fill 0
        for(int j=3; j>=0; j--)
        {
            result += ((temp>>j)&1) == 0 ? '0' : '1';
        }

    }
    bitset<64> data(result);
    return data;
}

/*
 *
 * Initial Permutation (IP)
 *
 */
bitset<64> plaintext_initial_permutation(bitset<64> str)
{
    bitset<64> result;
    for(int i=0; i<64; i++)
        result[63-i] = str[64 - T1[i]];
    return result;
}

// char -> binary
bitset<64> charToBitset(const char s[8])
{
	bitset<64> bits;
	for(int i=0; i<8; ++i)
		for(int j=0; j<8; ++j)
			bits[i*8+j] = ((s[i]>>j) & 1);
	return bits;
}


/**
 *  DES Encryption
 */
bitset<64> encrypt(bitset<64> str)
{

    // Step 1��Initial Permutation
    bitset<64> current_bits = plaintext_initial_permutation(str);
    cout<<"Initial Permutation result (64->64): "<<current_bits.to_string()<<endl;

    // Step 2��Group data, get left and right
    bitset<32> left;
    bitset<32> right;
    for(int i=32; i<64; i++)
        left[i-32] = current_bits[i];
    for(int i=0; i<32; i++)
        right[i] = current_bits[i];
    cout<<"L0:"<<left.to_string()<<endl;
    cout<<"R0:"<<right.to_string()<<endl;

    // Step 3��16 iterations in total
    bitset<32> new_left;
    for(int i=0; i<16; i++)
    {
        cout<<"No. "<<(i+1)<<" iteration"<<endl;
        new_left = right;
        right = left ^ f(right,Keys[i]);
        left = new_left;
        cout<<"L["<<i<<"]:"<<left<<endl;
        cout<<"R["<<i<<"]:"<<right<<endl<<endl;
    }

    // Step 4��Combine L16 and R16 to become R16L16
    bitset<64> cipher;
    for(int i=0; i<32; i++)
        cipher[i] = left[i];
    for(int i=32; i<64; i++)
        cipher[i] = right[i-32];
    cout<<"R16L16:"<<cipher.to_string()<<endl<<endl;

    // Step 5��Inverse IP
    current_bits = cipher;
    for(int i=0; i<64; i++)
        cipher[63-i] = current_bits[64-T6[i]];
    cout<<"Final permutation result: "<<cipher.to_string()<<endl<<endl;

    // Return the ciphertext
    return cipher;
}

/**
 *  DES Decryption
 */
bitset<64> decrypt(bitset<64> cipher)
{
    // Step 1��Initial Permutation
    bitset<64> current_bits = plaintext_initial_permutation(cipher);
    cout<<"The result of the IP when the ciphertext is treated as plaintext(64->64): "<<current_bits.to_string()<<endl<<endl;

    // Step 2��Get Li and Ri
    bitset<32> left;
    bitset<32> right;
    for(int i=32; i<64; i++)
        left[i-32] = current_bits[i];
    for(int i=0; i<32; i++)
        right[i] = current_bits[i];
    cout<<"L0:"<<left.to_string()<<endl;
    cout<<"R0:"<<right.to_string()<<endl;

    // Step 3��16 iterations in total��inverse use of subkeys��
    bitset<32> new_left;
    for(int i=0; i<16; i++)
    {
        cout<<"No. "<<(i+1)<<" iteration"<<endl;
        new_left = right;
        right = left ^ f(right,Keys[15-i]);
        left = new_left;
        cout<<"L["<<i<<"]:"<<left<<endl;
        cout<<"R["<<i<<"]:"<<right<<endl<<endl;
    }

    // Step 4��Combine L16 and R16 to become R16L16
    bitset<64> plain;
    for(int i=0; i<32; i++)
        plain[i] = left[i];
    for(int i=32; i<64; i++)
        plain[i] = right[i-32];
    cout<<"R16L16:"<<plain.to_string()<<endl<<endl;

    // Step 5��Inverse IP
    current_bits = plain;
    for(int i=0; i<64; i++)
        plain[63-i] = current_bits[64-T6[i]];
    cout<<"Final permutation result: "<<plain.to_string()<<endl<<endl;

    // Return the ciphertext
    return plain;
}


// Test
int main()
{
    string plain,key;
    cout<<"Please enter the plaintext��hex����"<<endl;
    getline(cin,plain);
    cout<<"Please enter the key��hex����"<<endl;
    getline(cin,key);

    cout<<endl<<endl<<endl<<"==========Begin generating the sub-keys==========="<<endl<<endl<<endl;
    // Key convert: Hex -> Binary
    bitset<64> key_bin = hex_2_bin(key);
    cout<<"Binary Key��"<<key_bin.to_string()<<endl;

    // Generate 16 sub-keys
    generateKeys(key_bin);
    cout<<endl<<endl<<endl<<"==========Finish generating the sub-keys==========="<<endl<<endl<<endl;


    cout<<endl<<endl<<endl<<"==========Begin Encryption==========="<<endl<<endl<<endl;
    // Plaintext convert: Hex -> Binary
    bitset<64> str_bin = hex_2_bin(plain);
    cout<<"Binary plaintext��"<<str_bin.to_string()<<endl;

    // Encryption
    bitset<64> result = encrypt(str_bin);
    cout<<"*******************Below is the ciphertext***********************"<<endl;
    cout<<"Cipher��Binary��:"<<result.to_string()<<endl;
    cout<<"Cipher��Hex����"<<bin_2_hex(result)<<endl<<endl<<endl<<endl;
    cout<<endl<<endl<<endl<<"==========Finish Encryption==========="<<endl<<endl<<endl;


    cout<<endl<<endl<<endl<<"==========Begin Decryption==========="<<endl<<endl<<endl;
    // Decryption
    result = decrypt(result);
    cout<<"*******************Below is the plaintext after decryption***********************"<<endl;
    cout<<"Plaintext��Binary��:"<<result.to_string()<<endl;
    cout<<"Plaintext��Hex����"<<bin_2_hex(result)<<endl;
    return 0;
}
